<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

class PoliciesController extends Controller
{
    public function coockiePolicy(){
        $pages = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->get();
        return view('policies.coockiePolicy')->with('pages', $pages);

    }

    public function legalData(){
        $pages = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->get();
        return view('policies.legalData')->with('pages', $pages);

    }
}
