<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\MailRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    public function submit(MailRequest $request) {

        try {
            $params = [
                'name' => $request->name,
                'phone' => $request->phone,
            ];
            Mail::send('mail', $params, function ($message) use ($request) {
                $message->to('vlad.t.ysbm@gmail.com')->subject('Request from your site'
                );
            });

            return back()->with('success','Your message has been sent, we will contact you shortly.');
        }
        catch (\Exception $e){
            return back()->with('error','Message was not sent');
        }
    }
}
