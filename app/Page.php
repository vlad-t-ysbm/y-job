<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title', 'slug', 'parent_id', 'post', 'main_page'];

    public function parents()
    {
        return $this->belongsTo('App\Page', 'parent_id')->get();
    }

    public function children()
    {
        return $this->hasMany('App\Page', 'parent_id')->get();
    }

}
