<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'description', 'short_description', 'author', 'reading_time', 'slug', 'topic_id'
    ];

    public function topics(){
        return $this->belongsTo('App\Topic', 'topic_id')->get();
    }
}
