<nav class="navbar navbar-expand-lg navbar-light bg-light" id="scrolled-nav">
    <div class="container">
        <div class="row w-100">
            <div class="col-lg-3">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler mob-menu" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="col-lg-7">
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav nav-center">
                        @foreach($pages as $page)
                            @if($page->children()->count())
                                <li class="nav-item dropdown mr-2">
                                    <a href="{{route('page', $page->slug)}}" class="dropbtn">{{$page->title}}</a>
                                    <button type="button" class="btn dropdown-toggle dropdown-toggle-split subpage-btn"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    @foreach($page->children() as $subpage)
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item"
                                           href="{{route('page', $subpage->slug)}}">{{$subpage->title}}</a>
                                    </div>
                                    @endforeach
                                </li>
                            @else
                            <li class="nav-item mr-2">
                                <a class="nav-link color-black" href="{{route('page', $page->slug)}}">{{$page->title}}<span class="sr-only">(current)</span></a>
                            </li>
                            @endif
                            @endforeach
                            <li class="nav-item mr-2">
                                <a class="nav-link color-black" href="{{route('blog.index')}}">Blog<span class="sr-only">(current)</span></a>
                            </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <button type="button" class="btn green-btn not-mobile nav-center" id="contact-btn" data-toggle="modal"
                        data-target="#exampleModal">Contact us
                </button>
            </div>
        </div>
    </div>
</nav>

