@foreach($data as $key => $value)
<div class="col-sm-12 col-md-12 col-lg-4 my-sm-1 my-md-1 my-lg-5">
    <img
        src="/img/articles/{{$value->image}}"
        onerror="this.src='https://place-hold.it/200x200';"
        class="blog-image">
</div>
<div class="col-sm-12 col-md-12 col-lg-8 my-5 text-left">
    <div class="d-flex text-item-color">
        <div class="mb-3 align-items-baseline d-flex">
            <div class="mr-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25"
                     class="text-black opacity-25">
                    <g fill="currentColor">
                        <path
                            d="M12.5 0C5.608 0 0 5.608 0 12.5S5.608 25 12.5 25 25 19.392 25 12.5 19.392 0 12.5 0zm0 22.34c-5.426 0-9.84-4.414-9.84-9.84s4.414-9.84 9.84-9.84 9.84 4.414 9.84 9.84-4.414 9.84-9.84 9.84z"></path>
                        <path
                            d="M18.936 11.908h-5.808V5.046c0-.578-.476-1.046-1.064-1.046C11.476 4 11 4.468 11 5.046v7.908c0 .578.476 1.046 1.064 1.046h6.872c.588 0 1.064-.468 1.064-1.046 0-.577-.476-1.046-1.064-1.046z"></path>
                    </g>
                </svg>
            </div>
            <div class="flex items-center default-text">
                Reading time: <span class="ml-1 inline-block font-bold font-weight-bold">{{$value->reading_time}}  minutes</span>
            </div>
        </div>
        <div class="flex ml-3 mb-3 align-items-center d-flex">
            <div class="mr-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="25" viewBox="0 0 23 25"
                     class="text-black opacity-25">
                    <path fill="currentColor" fill-rule="evenodd"
                          d="M2.846 22.1h17.307V9.594H2.846V22.1zM8.141 5.91c.784 0 1.423-.65 1.423-1.45h3.872c0 .8.639 1.45 1.423 1.45.785 0 1.424-.65 1.424-1.45h3.87v2.234H2.847V4.46h3.87c0 .8.64 1.45 1.425 1.45zm13.436-4.35h-5.294v-.11c0-.8-.639-1.45-1.424-1.45-.784 0-1.423.65-1.423 1.45v.11H9.564v-.11C9.564.65 8.925 0 8.141 0S6.717.65 6.717 1.45v.11H1.424C.64 1.56 0 2.21 0 3.01v20.539C0 24.349.639 25 1.424 25h20.153C22.36 25 23 24.349 23 23.549V3.009c0-.799-.639-1.45-1.423-1.45z"></path>
                </svg>
            </div>
            <div class="default-text">
                {{$value->updated_at->diffForHumans()}}
            </div>
        </div>
    </div>
    <div class="mb-3 blog-title">
        <a href="{{route('blog.show', $value->slug)}}">
            <span> {{$value->title}} </span>
        </a>
    </div>
    <div class="mb-3 text-item-color description-font">
        {!! \Illuminate\Support\Str::words($value->short_description, 85,'...') !!}
    </div>
    <div class="text-item-color">
         {{$value->author}}
    </div>
</div>
@endforeach
