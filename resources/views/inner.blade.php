@extends('layouts.app')

@section('content')
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="post">
                {!! $page->post !!}
                </div>
            </div>
        </div>
    </div>
@endsection
