@extends('layouts.app')

@section('content')
    <section class="section-item section-bg">
        <div class="container">
        <div class="row main-title">
            <div class="col-sm-12 col-mb-12 col-lg-9">
                <div>
                    <h1>Legalizacja pracownikow z Ukrainy i Europa Wschodnej dla tvoego biznesu</h1>
                </div>
                <div class="section-ul-item ml-5">
                    <ul class="list-unstyled">
                        <li>Legalizacja</li>
                        <li>Recruting</li>
                        <li>Wynajem pracownikow</li>
                        <li>Kompleksowa obsluga pracownika</li>
                    </ul>
                </div>
            </div>
            <div class="col-3">

            </div>
        </div>
        </div>
    </section>
    <section class="section-2">
        <div class="text-uppercase section-2-items">
            <span>5 lat na rynku</span>
            <span>10 000 candidatow w bazy</span>
            <span>5 000 zyskanych zezwolen</span>
            <span>Wspolpraca z 15 krajami</span>
        </div>
    </section>
    <section class="section-3 py-5 section-3-items">
        <div class="container">
            <div class="row">
                <div class="col-12 my-3 ">
                    <h1>Test Title for 3-th section</h1>
                </div>
                <div class="col-4 my-3 border-item">
                    <div class="pl-5">
                    <span class="title-item">TITLE</span>
                    <ul class="list-item mt-3">
                        <li><p>Test sentence Test sentence Test sentence Test sentence</p></li>
                        <li><p>Test sentence Test sentence Test sentence</p></li>
                        <li><p>Test sentence Test sentence</p></li>
                    </ul>
                    </div>
                </div>
                <div class="col-4 my-3 border-item">
                    <div class="pl-5">
                    <span class="title-item">TITLE</span>
                    <ul class="list-item mt-3">
                        <li><p>Test sentence Test sentence Test sentence</p></li>
                        <li><p>Test sentence Test sentence Test sentence</p></li>
                        <li><p>Test sentence Test sentence</p></li>
                        <li><p>Test sentence Test sentence</p></li>
                        <li><p>Test sentence</p></li>
                    </ul>
                    </div>
                </div>
                <div class="col-4 my-3 border-item">
                    <div class="pl-5">
                    <span class="title-item">TITLE</span>
                    <ul class="list-item mt-3">
                        <li><p>Test sentence Test sentence Test sentence</p></li>
                        <li><p>Test sentence Test sentence</p></li>
                        <li><p>Test sentence Test sentence</p></li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
