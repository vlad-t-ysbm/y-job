<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Contact us</h3>
{{--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                    <span aria-hidden="true">&times;</span>--}}
{{--                </button>--}}
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('send-mail')}}">
                    @csrf
                    @method('post')
                    <div class="form-group">
                        <input type="text" class="form-control form-wrap-item border-0" id="name" name="name" placeholder="Enter your name" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control form-wrap-item border-0" id="phone" name="phone" placeholder="Enter your phone" required>
                    </div>
                    @if($errors)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="rules-recapcha-wrapper w-100">
                        <input class="rules-recapcha-checkbox" type="checkbox" required>
                        <span class="fw-500">I agree to the processing of personal data</span>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green-btn contact-btn">Submit</button>
                    </div>
                    <div class="w-100 form-number">
                        <span class="fw-500">Or call the number</span>
                        <p class="font-weight-bold">
                            <a href="tel:780 060 917" class="modal-phone">780 060 917</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
