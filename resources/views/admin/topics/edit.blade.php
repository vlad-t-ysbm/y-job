@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('topics.update', $topic->id)}}">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleFormControlInput1">Title</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title" value="{{$topic->title}}">
        </div>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
@endsection
