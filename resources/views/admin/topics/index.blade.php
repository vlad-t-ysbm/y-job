@extends('layouts.admin-app')

@section('content')
    <a href="{{route('topics.create')}}" class="btn btn-info mb-1 w-100">Create topic</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Article</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($topics as $key => $value)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$value->title}}</td>
            <td>
                <ul class="p-0">
            @foreach($value->articles() as $article)
                        <li><a href="{{route('blog.show', $article->slug)}}">{{$article->title}}</a></li>
                @endforeach
                </ul>
            </td>
            <td class="d-flex">
                <a class="btn btn-warning action-button" href="{{route('topics.edit', $value->id)}}">Edit</a>
                <form method="post" action="{{route('topics.destroy', $value->id)}}">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger action-button ml-2" type="submit">Delete</button>
                </form>
            </td>
        </tr>
            @endforeach
        </tbody>
    </table>
@endsection
