@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('topics.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlInput1">Title</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title">
        </div>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
@endsection
