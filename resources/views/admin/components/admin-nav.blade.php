<nav>
    <div class="admin-menu-item">
        <a href="{{route('pages.index')}}">
            PAGES
        </a>
    </div>
    <div class="admin-menu-item">
        <a href="{{route('articles.index')}}">
            ARTICLES
        </a>
    </div>
    <div class="admin-menu-item">
        <a href="{{route('topics.index')}}">
            TOPICS
        </a>
    </div>
    <div class="admin-menu-item">
        <a href="/">
            MAIN PAGE
        </a>
    </div>
</nav>
