@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('articles.update', $article->id)}}" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleFormControlInput1">Title</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" value="{{$article->title}}" name="title">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Slug</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" value="{{$article->slug}}" name="slug">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Image</label>
            <div>
                <label>Current image</label>
            <img class="d-block mb-2" src="/img/articles/{{$article->image}}" width="200" height="200">
            </div>
            <input type="file" class="form-control-file" id="exampleFormControlFile1" value="" name="image">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Description</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="5">{{$article->description}}</textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea2">Short description</label>
            <textarea class="form-control" id="exampleFormControlTextarea2" name="short_description" rows="5">{!! $article->short_description !!}</textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput2">Author</label>
            <input type="text" class="form-control" id="exampleFormControlInput2" name="author" value="{{$article->author}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput3">Reading time</label>
            <input type="text" class="form-control" id="exampleFormControlInput3" name="reading_time" value="{{$article->reading_time}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Topic</label>
            {{$article->topic_id}}
            <select class="form-control" id="exampleFormControlSelect1" name="topic_id">
                <option value="0">None</option>
                @foreach($topics as $topic)
                    @if($topic->id == $article->topic_id)
                        <option value="{{$topic->id}}" selected>{{$topic->title}}</option>
                    @else
                    <option value="{{$topic->id}}">{{$topic->title}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-success">SAVE</button>
    </form>
@endsection


