@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('articles.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlInput1">Title</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Slug</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="slug">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Image</label>
            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Description</label>
            <textarea class="form-control post" id="exampleFormControlTextarea1" name="description" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea2">Short description</label>
            <textarea class="form-control post" id="exampleFormControlTextarea2" name="short_description" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput2">Author</label>
            <input type="text" class="form-control" id="exampleFormControlInput2" name="author">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput3">Reading time</label>
            <input type="text" class="form-control" id="exampleFormControlInput3" name="reading_time">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Topic</label>
            <select class="form-control" id="exampleFormControlSelect1" name="topic_id">
                <option value="0">None</option>
                @foreach($topics as $topic)
                <option value="{{$topic->id}}">{{$topic->title}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-success">SAVE</button>
    </form>
@endsection
