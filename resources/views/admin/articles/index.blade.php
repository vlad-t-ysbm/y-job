@extends('layouts.admin-app')

@section('content')
    <a href="{{route('articles.create')}}" class="btn btn-info mb-1 w-100">Create article</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Short description</th>
            <th scope="col">Image</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $key => $value)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$value->title}}</td>
            <td>{!!Str::limit($value->short_description, 200)!!}</td>
            <td><img src="/img/articles/{{$value->image}}" width="100px" height="100px"></td>
            <td class="d-flex">
                <a class="btn btn-warning action-button" href="{{route('articles.edit', $value->id)}}">Edit</a>
                <form method="post" action="{{route('articles.destroy', $value->id)}}">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger action-button ml-2" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
