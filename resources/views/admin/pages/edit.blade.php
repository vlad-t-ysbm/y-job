@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('pages.update', $page->id)}}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleFormControlInput1">Page title</label>
            <input class="form-control" name="title" type="text" placeholder="Page title" value="{{$page->title}}" required>
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Slug</label>
            <input class="form-control" name="slug" type="text" placeholder="Slug" value="{{$page->slug}}" id="slug" required>
        </div>
        <div class="form-group">
            <div class="form-check">
                @if($page->main_page === 1)
                    <input class="form-check-input" type="checkbox" id="invalidCheck2" value="1" name="main_page" checked>
                    @else
                    <input class="form-check-input" type="checkbox" id="invalidCheck2" value="1" name="main_page">
                    @endif
                <label class="form-check-label" for="invalidCheck2">
                    Make it main page?
                </label>
            </div>
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Page parrent</label>
            @if($page->children()->count())
                <p class="warning">
                    Sorry, you cannot select the parent page for the parent page.
                </p>
            @else
            <select class="form-control" name="parent_id">
                <option value="0">None</option>
                @foreach($pages as $item)
                    @if($page->parent_id == $item->id)
                        <option value="{{$item->id}}" selected>{{$item->title}}</option>
                    @else
                        @if($page->id == $item->id)
                        @else
                        <option value="{{$item->id}}">{{$item->title}}</option>
                        @endif
                    @endif
                @endforeach
            </select>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Post</label>
            <textarea class="form-control editor" id="exampleFormControlTextarea1" name="post" rows="5">{!!$page->post!!}</textarea>
        </div>
        <a href="{{route('pages.index')}}" class="btn btn-secondary">Back</a>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
@endsection
