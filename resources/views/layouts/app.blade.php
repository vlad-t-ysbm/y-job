<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('components.head')
</head>
<body>
    <div id="app">
        <div class="bg">
        @include('components.nav')
        @include('modal')
        <main class="">
            @include('components.flash-messages')
            @yield('content')
        </main>
        @include('components.footer')
        </div>
    </div>
    @include('components.scripts')
</body>
</html>
