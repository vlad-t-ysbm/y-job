<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('components.head')
</head>
<body>
<div id="app">
    @include('components.nav')
    @include('modal')
    <main class="py-5">
        @include('components.flash-messages')
        @yield('content')
    </main>
    @include('components.footer')
</div>
@include('components.scripts')
</body>
</html>
