<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
@include('admin.components.head')
</head>
<body>
    <div id="app">
        <main class="py-4 px-4">
            <div class="row">
                <div class="col-md-2 admin-menu">
                    @include('admin.components.admin-nav')
                </div>
                <div class="col-md-10">
                    @include('admin.components.flash-messages')
                    @yield('content')
                </div>
            </div>
        </main>
    </div>
@include('admin.components.scripts')
</body>
</html>
