@extends('layouts.blog-app')

@section('content')
    <div class="container my-5 text-center">

        <div class="h1">
            Blog
        </div>
        <form class="mb-5 lg:mb-8">
            <div class="my-2 my-4">
                <label class="search-input flex items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         class="search-input__icon">
                        <path fill="currentColor"
                              d="M23.635 21.86l-5.916-5.94a9.46 9.46 0 0 0 2.354-6.23c0-5.343-4.502-9.69-10.036-9.69S0 4.347 0 9.69c0 5.343 4.503 9.69 10.037 9.69a10.18 10.18 0 0 0 5.75-1.753l5.961 5.985c.25.25.585.388.944.388.34 0 .662-.125.907-.353.52-.483.537-1.284.036-1.787zM10.037 2.528c4.09 0 7.418 3.213 7.418 7.162 0 3.95-3.328 7.162-7.418 7.162S2.618 13.64 2.618 9.69c0-3.95 3.328-7.162 7.419-7.162z"></path>
                    </svg>
                    <input placeholder="Blog search" id="search" name="search"
                           class="search-bg pl-7 pr-5 outline-none search-item"></label></div>
            <div class="row justify-content-center">
                <div class="px-1 py-1">
                    <button type="button" class="button button--default selected-topic tag-button active"
                            data-topic-id="0">
                        ALL
                    </button>
                </div>
                @if($topics)
                    @foreach($topics as $topic)
                        <div class="px-1 py-1">
                            <button type="button" class="button button--default selected-topic tag-button" id="topic"
                                    data-topic-id="{{$topic->id}}">
                                {{$topic->title}}
                            </button>
                        </div>
                    @endforeach
                @else
                @endif
            </div>
        </form>
        <div class="panel-heading">
        </div>
        <div class="panel-body">
            {{ csrf_field() }}
            <div class="row">
                <div id="post_data" class="d-flex flex-wrap">
                @include('components.article')
                </div>
                <div id="load_more" class="text-left col-12">
                    <button type="button" name="load_more_button" class="load-more" id="load_more_button"
                            data-page="1" data-pages=""
                    >Load More
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
