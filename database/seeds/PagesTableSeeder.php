<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([[
            'title' => 'First test page',
            'parent_id' => 0,
            'post' => '<p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500" alt="" /></p>
<h1 style="text-align: center;">1st post by tinemce editor</h1>
<h3>There is content of test post&nbsp;</h3>
<h2><strong>TEST BOLD&nbsp;</strong></h2>
<p><strong><a href="https://tjd-studio.com/blog/how-to-install-tinymce-on-laravel-55" target="_blank" rel="noopener">LINK TEST</a></strong></p>',
            'slug' => 'first-test-page',
            'main_page' => '1',
        ],
            [
                'title' => 'First test page',
                'parent_id' => 1,
                'post' => 'Test children page',
                'slug' => 'test-children-page',
            ],
        ]);
    }
}
