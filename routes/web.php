<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Site
Route::get('/', 'Blog\BlogController@index');
Route::get('/page/{slug}', 'Blog\BlogController@show')->name('page');
Route::resource('/blog','Articles\ArticlesController');
//Route::get('/blog/topic/{slug}', 'Articles\ArticlesController@showBySlug')->name('show-by-slug');
//Route::post('/blog/topic/{slug}', 'Articles\ArticlesController@loadBySlug')->name('show-by-slug-topic');
Route::get('/coockie-policy', 'Blog\PoliciesController@coockiePolicy')->name('coockie-policy');
Route::get('/legal-data', 'Blog\PoliciesController@legalData')->name('legal-data');
Route::post('/blog/load_data', 'Articles\ArticlesController@load_data')->name('load_data');
//Route::post('/blog/by-topic', 'Articles\ArticlesController@getByTopic')->name('by-topic');
Auth::routes();

// Auth
Route::get('/register', 'Admin\AdminController@redirect');
Route::get('/password/reset', 'Admin\AdminController@redirect');
Route::get('/admin', 'Admin\AdminController@admin');
Route::post('/submit','Blog\SendMailController@submit')->name('send-mail');
// Admin
Route::group(['middleware'=>'auth', 'prefix'=>'admin'],function () {
    Route::resource('/pages', 'Admin\PagesController');
    Route::resource('/articles', 'Admin\ArticlesController');
    Route::resource('/topics', 'Admin\TopicsController');
    Route::post('/pages/{page}/set-main-page', 'Admin\PagesController@setMainPage')->name('set-main-page');
});



